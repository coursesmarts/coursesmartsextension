/* UG registration system content script. This script only runs on UG course pages and UG course search pages */

/* Function Declaration and Implementation */
/* ====================================================================================================== */
// Convert semester number to its name string, according to the convention specified in the README.md
function semNum2Name(num) {
	num = parseInt(num);
	switch(num){
		case 1:
			return "חורף";
		case 2:
			return "אביב";
		case 3:
			return "קיץ";
	}
}

// Small jQuery trick to find out if an element exists on the page
$.fn.exists = function () {
    return this.length !== 0;
};

// The first function of retrieving the lecturer survey data.
// The function creates a global array called "lecturers" with links to their survey page
// Once the array is ready, it calls getMishals with this array.
// If there's no answer for 30 seconds, then declare an error to the user
function loadMishalGrades(){
	$( "a[href*='cics']" ).each(function(id){
		//console.log($(this).attr("href"));
		lecturers.push($(this).attr("href"));
		$( "<span class='badge badge-success' style='margin-right: 5px;' id='lect_"+id+"'></span>" ).insertAfter( $(this) );
	});
	//console.log(lecturers);
	$( "td:contains('מרצה')" ).html("מרצה/מתרגל<span class='label label-info' style='margin-right: 5px;' id='lect_load'> טוען נתונים...</span>");
	timeout=1;
	getMishals(lecturers);
	setTimeout(function(){ 
					if (timeout) {
						$( "td:contains('מרצה')" ).html("מרצה/מתרגל<span class='label label-danger' style='margin-right: 5px;' id='lect_load'> שגיאה</span>");
					}
	}, 30000);
}

// Retreive the grades for the lecturers that were found in the page earlier. Receives an array of urls, earlier called "lecturers".
// The format of the response is:
// gradeOfUrl0_gradeOfUrl1_gradeOfUrl2_... The order is the same order of the array and the page, so it's convenient to inject into the page. and yep, it's ugly.
// If there's not grade for a certain lecturer (or teaching assistant) it shows "empty" in the list mentiones above.
// The grades are injected into the page and colored in the following way:
// Above 4: Green, Between 3 and 4: Yellow, Below 3: Red
function getMishals(urls) {
   	chrome.runtime.sendMessage({type: "getMishal", id: globid, pass: globpass, urls: urls, count: urls.length}, function(response) {
   		timeout=0;
		mishal_data = response.result;
		//console.log("Data: "+mishal_data);
		if (mishal_data === "") {
			$( "td:contains('מרצה')" ).html("מרצה/מתרגל<span class='label label-warning' style='margin-right: 5px;' id='lect_load'> שגיאה</span>");
		}
		if (mishal_data !== "empty") {
			var data = mishal_data.split("_");
			for (i=0 ; i<urls.length ; i++){
				//console.log(data[i]);
				$('#lect_load').html('');
				if (data[i] !== "empty"){
					if (parseFloat(data[i]) < 4 && parseFloat(data[i]) > 3){
						$('#lect_'+i+'').attr( "class", "badge badge-warning" );
					}
					if (parseFloat(data[i]) <= 3){
						$('#lect_'+i+'').attr( "class", "badge badge-important" );
					}
					$('#lect_'+i+'').html(data[i]);
				}
			}
		} 
	}); 
}

// Check if the username and password the user provided for the UG system is correct.
// Return the result to the callback function: 1 for correct and 0 for incorrect.
// If the credentials are correct, we save them in the extension's storage using saveMishalCreds()
function checkMishalCreds(id, pass, callback) {
	var verified;
   	chrome.runtime.sendMessage({type: "verifyMishCreds", id: id, pass: pass}, function(response) {
		var data = response.result;
		console.log("Cred Valid: "+data);
		if (data === "0") {
			//console.log("1verified = 0");
			callback(0);
		} else if (data === "1") {
			saveMishalCreds(id, pass);
			callback(1);
		}
	});
}

// Saves the given credentials in the extension's storage.
function saveMishalCreds(id, pass) {
	chrome.storage.sync.set({mishalid: id, mishalpass: pass});
	globid = id;
	globpass = pass;

}

// Check if the user already supplied his credentials to log into the UG system.
// If he did, calls the callback function with 1, else calls it with 0.
function haveMishalCreds(callback) {
	chrome.storage.sync.get(['mishalid', 'mishalpass'], function(items) {
	    mishalid = items.mishalid;
	    mishalpass = items.mishalpass;
	    if (mishalid && mishalpass) {
	        //console.log("Creds: id-"+mishalid+"pass-"+mishalpass);
	        globid = mishalid;
	        globpass = mishalpass;
	        callback(1);
	    } else {
	    	console.log("No creds");
	    	callback(0);
	    }
	});
}

// Check what is the average of the "course" when the "lecturer" was in charge.
// This data is maintained in the server when inserting new averages to the system
// The data returned is either the average of the total grade, or only Moed A, depending on the data we posses.
// 0 represents no data, 1 followed by "_" and grade represents total average, and 2 followed by "_" and grade is for Moed A average.
// The data is then inserted into the appropriate place in the page.
function getLecturerAvg(lecturer, course) {
   	chrome.runtime.sendMessage({type: "getLecturerAvg", lecturer: lecturer, course: courseNum}, function(response) {
		var lect_data = response.result;
		//console.log("Data: "+lect_data);
		if (lect_data === "0") {
			return;
		}
		lect_data_arr = lect_data.split('_');
		if (lect_data_arr[0] === "1") {
			var avg = Math.round(lect_data_arr[1]*100)/100;
			$( "div:contains('אחראים')" ).next(".property-value").html(lecturer_in_charge+"<br> <b>ממוצע עבור המרצה האחראי הנ\"ל:</b> <span class='badge badge-info'>"+avg+"</span> ("+lect_data_arr[2]+")");
		}
		if (lect_data_arr[0] === "2") {
			var avg = Math.round(lect_data_arr[1]*100)/100;
			$( "div:contains('אחראים')" ).next(".property-value").html(lecturer_in_charge+"<br> <b>ממוצע מועדי א\' עבור המרצה האחראי הנ\"ל:</b> <span class='badge badge-info'>"+avg+"</span> ("+lect_data_arr[2]+")");
		}

	}); 	
}

// Rerieve the students' ratings for the course.
// If "0" is returned, there are no ratings for this course.
// Otherwise, the rating and the number of users who rated is returned, divided by spaces.
function getRatings() {
	chrome.runtime.sendMessage({type: "getRating", course: courseNum}, function(response) {
		rate_data = response.result;
		//console.log(rate_data);
		if (rate_data === "0") {
			$("#reviews_line").html("הקורס עדין לא דורג");
		} else {
			rate_data = rate_data.split(' ');
			var rating = parseFloat(rate_data[1]);
			rating = Math.round(rating*2)/2;
			var num_ratings = parseInt(rate_data[2]);
			$("#stars").rateit('value', rating);
			$("#stars").css("display", "inline");
			$("#rate_text").html(' ('+num_ratings+')');
			$("#rate_link").html('<a href="" id="mod2" class="label label-success" style="width: 85px; text-align: center; margin-left: 5px;s">קריאת חוות דעת</a> ');
		}
	});
}

// Retrieve the average of the course. This data depends on what data we posses on our server.
// Therefore, there are several options:
// 1: Total average within the last 3 years
// 2: Moed A average within the last 3 years
// 3: Only one grade which can be a total averge, Moed A or Moed B.
// 5: There are no grades and no histograms in the system.
// All grades are rounded to the nearest 1/100 point.
function getAverages(){
	chrome.runtime.sendMessage({type: "getAverage", course: courseNum}, function(response) {
		avg_data = response.result;
		console.log(avg_data);
		if (avg_data === "5") {
			$("#grades_line").html("אין היסטוגרמות וציונים במערכת");
			return;
		}

		if (avg_data === "0") {
			$("#grades_data").html("");
		} else {
			avg_data = avg_data.split(' ');
			if (avg_data[0] === "1") {
				var avg = Math.round(avg_data[1]*100)/100;
				$("#grades_data").html("<b>שקלול ממוצע סופי מה3 שנים האחרונות:</b> <b class='badge badge-info'>"+avg+"</b> ("+avg_data[2]+") ");
			}
			else if (avg_data[0] === "2") {
				var avg = Math.round(avg_data[1]*100)/100;
				$("#grades_data").html("<b>שקלול ממוצעי מועדי א' סופי מה3 שנים האחרונות:</b> <b class='badge badge-info'>"+avg+"</b> ("+avg_data[2]+") ");
			}
			else if (avg_data[0] === "3") {
				var moed;
				if (avg_data[1] === "T") {
					moed="סופי";
				} else if (avg_data[1] === "A") {
					moed="מועד א' סופי";
				} else if (avg_data[1] === "B") {
					moed="מועד ב' סופי";
				}
				var avg = Math.round(avg_data[2]*100)/100;
				var year = avg_data[3];
				var sem = semNum2Name(avg_data[4]);
				$("#grades_data").html("<b>ממוצע "+moed+" "+sem+" "+year+":</b> <b class='badge badge-info'>"+avg+"</b>");
			}				
		}
	});
}

// This function is used by the functions that load the number of seats remaining in a course
// The result from the server is "0" if there are no seats at all, -1 and -2 for UG errors, and list of "_" seperated JSON objects
// that represent a course group and its remaining free seats.
// This function calls a callback with an array in which the index is the group number and the data is the seats.
// The course number in the callback argument is there for cases in which there are many calls to this func. with different courses
function getSeatsArr(courseNum, callback) {
	var arr = [];
	chrome.runtime.sendMessage({type: "getSeatsLeft", id: globid, pass: globpass, course: courseNum}, function(response) {
		result = response.result;
		//console.log(result);
		if (result == 0){
			callback(0, courseNum);
		}
		if (result == -1 || result == -2){
			callback(result, courseNum);
		}
		result = result.split('_');
		for (i=0 ; i<result.length-1 ; i++) {
			var obj = jQuery.parseJSON(result[i]);
			var group = parseInt(obj.group);
			var seats = parseInt(obj.seats);
			arr[group] = seats;
		}
		callback(arr, courseNum);
	});
}

// This function is called only when the user has provided a verified credentials to the UG
// It works only on the course page
// It uses the function getSeatsArr() in order to receive the free seats, and then injects the data into the course page
// For empty groups we'll have a red background, for less than 5 seats yellow background, and more than 5 green background
function loadSeatsToCoursePage(){
	if (search_page == 0) {
		var numGroups = 0;
		$( "a[href*='vacancy']" ).each(function(id){
			numGroups++;
		});
		$( "a[href*='vacancy']" ).each(function(id){
			if (id === numGroups-1 || numGroups === 0) {
				return;
			}
			id=id+1;
			$("<img id='l"+id+"' src='"+chrome.extension.getURL("/img/loading1.gif")+"' style='height: 15px; margin-right:5px;'></img>").insertAfter($(this));
		});
		if (numGroups > 0) {
			getSeatsArr(courseNum, function(arr, courseNum) {
				// console.log(arr);
				if (arr == -1 || arr == -2) {
					if (arr == -1){
						message = "UG אינו פעיל";
					} else if (arr == -2) {
						message = "יותר מדיי בקשות"
					}
					$(".rishum-groups tbody").children().each(function(id, element){
						if (id === 0) {
							return;
						}
						$("<b class='badge badge-important' data-toggle='tooltip' data-placement='top' title='UG אינו פעיל: לא ניתן לקבל את המידע מכיוון שהUG הלך לישון. יותר מדיי בקשות: הUG חסם את גישתך לבדיקת מקומות פנויים באופן זמני, עקב ריבוי פניות' style='margin-right:5px;'>"+message+"</b>").insertAfter($("#l"+id));
						$("#l"+id).remove();
					});
					$('[data-toggle="tooltip"]').tooltip();
					return;
				}
				$(".rishum-groups tbody").children().each(function(id, element){
					// console.log(arr);
					if (id === 0) {
						return;
					}
					if (arr == 0) {
						$("<b class='badge badge-important' style='margin-right:5px;'>0</b>").insertAfter($("#l"+id));
						$("#l"+id).remove();
						return;
					}
					group = parseInt($(this).children().first().text());			
					if (typeof arr[group] == 'undefined') {
						arr[group] = 0;
					}
					var type;
					// console.log(arr[group]);
					if (arr[group] == 0) {
						type = "important";
					} else if (arr[group] < 5) {
						type = "warning";
					} else {
						type = "success";
					}
					$("<b class='badge badge-"+type+"' style='margin-right:5px;'>"+arr[group]+"</b>").insertAfter($("#l"+id));
					$("#l"+id).remove();
				});
			});	
		}
	}
}

// This function is similar to loadSeatsToCoursePage(). The major difference is this function is called for loading free seats into the
// search page and not a course page. The number of free seats is a sum of all the free seats in all of the groups.
// The UG server blocks student IDs that create too many requests to the server.
// To avoid being blocked, we've added a recursive mechanism that will initiate a request to the UG server every 1.5 seconds.
// This affects the time it takes to inject the numbers, but we don't want the UG to block the student's ID. (It might happen anyway)
// In addition, we stop fetching the number of seats left after fetching 20 courses. It's for the user's own good! :)
function addSeatsLeftToSearch() {
	if ($(".result-row").exists() || document.title.indexOf("חיפוש") != -1) {
		search_page = 1;

		var links = $( "a[href*='vacancy']" );
		for (i=0 ; i<links.length ; i++) {
			links.eq(i).prev().remove();
		}
		var links_length = links.length;
		var counter = 1;
		var cont_flag = 0;
		const MAX_QUERIES = 50;
		const CALLS_BEFORE_STOP = 10;
		function recursiveAddSeatsLeftInSearch() {
			if ($('#contbtn').exists()) {
				$('#contbtn').tooltip('hide');
			}
			if (counter % CALLS_BEFORE_STOP === 0 && cont_flag === 0) {
				$("<button id='contbtn' class='btn btn-info btn-small' style='margin-right:5px;' data-toggle='tooltip' data-placement='left' title='זהירות! בדיקת מקומות בקורסים רבים תגרום לחסימה. כדאי להקטין את מספר תוצאות החיפוש.'>טען עוד</button>").insertAfter(links.eq(counter-1));
				$('#contbtn').click(recursiveAddSeatsLeftInSearch);
				$('#contbtn').tooltip();
				cont_flag = 1;
				return;
			}
			if (counter <= links_length && counter < MAX_QUERIES) {
				cont_flag = 0;
				if ($('#contbtn').exists()) {
					$('#contbtn').remove();
				}
				tmp_course = links.eq(counter-1).attr("href").split("/");
				tmp_course = parseInt(tmp_course[tmp_course.length-1]);
				//console.log(tmp_course);
				$("<img id='l"+tmp_course+"' src='"+chrome.extension.getURL("/img/loading1.gif")+"' style='height: 15px; margin-right:5px;'></img>").insertAfter(links.eq(counter-1));
				
				getSeatsArr(tmp_course, function(arr, tmp_course1) {
					//console.log(arr);
					var total_seats;
					if (arr == 0) {
						total_seats = 0;
					} else if (arr == -1) {
						total_seats = "UG אינו פעיל";
					} else if (arr == -2) {
						total_seats = "יותר מדיי בקשות";
					} else {
						for (i=0 ; i<arr.length ; i++) {
							if (arr[i] == undefined) {
								arr[i] = 0;
							}
						}
						total_seats = arr.reduce(getSum);
					}
					var type;
					if (total_seats == 0 || total_seats == "UG אינו פעיל" || total_seats == "יותר מדיי בקשות") {
						type = "important";
					} else if (total_seats < 5) {
						type = "warning";
					} else {
						type = "success";
					}
					$("<b class='badge badge-"+type+"' style='margin-right:5px;'>"+total_seats+"</b>").insertAfter($("#l"+tmp_course1));
					$("#l"+tmp_course1).remove();
				});

				counter++;
				setTimeout(recursiveAddSeatsLeftInSearch, 1000);
			}
		}
		recursiveAddSeatsLeftInSearch();
	}
}

function getSum(total, num) {
    return total + num;
}

function pastLecturersHTML(semesters) {
	try {
		var V = "<img src='data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDI2IDI2IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNiAyNiIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCI+CiAgPGc+CiAgICA8cGF0aCBkPSJtMTMsMGMtNy4yLDAtMTMsNS44LTEzLDEzczUuOCwxMyAxMywxMyAxMy01LjggMTMtMTMtNS44LTEzLTEzLTEzem0wLDIyYy01LDAtOS00LTktOXM0LTkgOS05IDksNCA5LDktNCw5LTksOXoiIGZpbGw9IiM5MURDNUEiLz4KICAgIDxwYXRoIGQ9Im0xNy4zLDkuM2MtMC4yLTAuMi0wLjUtMC4zLTAuNy0wLjNzLTAuNSwwLjEtMC43LDAuM2wtMy42LDMuM2MtMC4yLDAuMi0wLjUsMC4yLTAuNywwbC0xLjUtMS4zYy0wLjQtMC40LTEtMC40LTEuNCwwbC0xLjQsMS40Yy0wLjIsMC4yLTAuMywwLjUtMC4zLDAuN3MwLjEsMC41IDAuMywwLjdsNCwzLjZjMC4yLDAuMiAwLjQsMC4zIDAuNywwLjMgMC4zLDAgMC41LTAuMSAwLjctMC4zbDYtNS42YzAuMi0wLjIgMC4zLTAuNSAwLjMtMC43cy0wLjEtMC41LTAuMy0wLjdsLTEuNC0xLjR6IiBmaWxsPSIjOTFEQzVBIi8+CiAgPC9nPgo8L3N2Zz4K' />";
		var X = "<img src='data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDI2IDI2IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNiAyNiIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCI+CiAgPGc+CiAgICA8cGF0aCBkPSJtMTMsMGMtNy4yLDAtMTMsNS44LTEzLDEzczUuOCwxMyAxMywxMyAxMy01LjggMTMtMTMtNS44LTEzLTEzLTEzem0wLDIyYy01LDAtOS00LTktOXM0LTkgOS05IDksNCA5LDktNCw5LTksOXoiIGZpbGw9IiNEODAwMjciLz4KICAgIDxwYXRoIGQ9Im0xNy43LDExLjFjMC4yLTAuMiAwLjMtMC41IDAuMy0wLjdzLTAuMS0wLjUtMC4zLTAuN2wtMS40LTEuNGMtMC4yLTAuMi0wLjUtMC4zLTAuNy0wLjNzLTAuNSwwLjEtMC43LDAuM2wtMS41LDEuNWMtMC4yLDAuMi0wLjUsMC4yLTAuNywwbC0xLjUtMS41Yy0wLjItMC4yLTAuNS0wLjMtMC43LTAuM3MtMC41LDAuMS0wLjcsMC4zbC0xLjQsMS40Yy0wLjIsMC4yLTAuMywwLjUtMC4zLDAuN3MwLjEsMC41IDAuMywwLjdsMS41LDEuNWMwLjIsMC4yIDAuMiwwLjUgMCwwLjdsLTEuNSwxLjVjLTAuMiwwLjItMC4zLDAuNS0wLjMsMC43czAuMSwwLjUgMC4zLDAuN2wxLjQsMS40YzAuMiwwLjIgMC41LDAuMyAwLjcsMC4zIDAuMywwIDAuNS0wLjEgMC43LTAuM2wxLjUtMS41YzAuMi0wLjIgMC41LTAuMiAwLjcsMGwxLjUsMS41YzAuMiwwLjIgMC41LDAuMyAwLjcsMC4zIDAuMywwIDAuNS0wLjEgMC43LTAuM2wxLjQtMS40YzAuMi0wLjIgMC4zLTAuNSAwLjMtMC43cy0wLjEtMC41LTAuMy0wLjdsLTEuNS0xLjVjLTAuMi0wLjItMC4yLTAuNSAwLTAuN2wxLjUtMS41eiIgZmlsbD0iI0Q4MDAyNyIvPgogIDwvZz4KPC9zdmc+Cg==' />";
		// console.log(semesters);
		var years = [];
		var sems = [];
		var lects = [];
		for (var sem in semesters) {
			var year, sem, lect;
		    if (semesters.hasOwnProperty(sem)) {
		        var lectsdata = semesters[sem];
		        // console.log(semesters[sem]);
		        if (typeof lectsdata !== 'object') {
		        	continue;
		        }
		        year = parseInt(sem.substring(0,4))+1;
		        sem = semNum2Name(parseInt(sem.substring(5,6)));
		        // console.log("Year: "+year+" Sem: "+sem);
		        years.push(year);
		        sems.push(sem);
		        if (lectsdata.occured === false) {
		        	lects.push("0");
		        } else {
		        	lectsarr = lectsdata.lecturers;
		        	var fl = 0;
		        	for (var i=0 ; i < lectsarr.length ; i++) {
		        		if (lectsarr[i].incharge === true) {
		        			lects.push(lectsarr[i].name);
		        			fl=1;
		        		}
		        	}
		        	if (fl === 0) {
		        		lects.push(" ");
		        	}
		        }
		    }
		}
		var lects_html = "<center><table class='table-bordered' style='font-size: small; table-layout: fixed;' width='100%'><tr>";
		if (years.length < 6) {
			tablelen = years.length;
		} else {
			tablelen = 6;
		}
		for (var i=years.length-1, j=0 ; j<tablelen ; j++, i--) {
			lects_html += "<th style='width: 16%;'>"+sems[i]+" "+years[i]+"</th>";
		}
		lects_html += "</tr><tr>";
		for (var i=lects.length-1, j=0 ; j<tablelen ; j++, i--) {
			lects_html += "<td style='text-align: center; width: 16%;'>";
			if (lects[i] === "0") {
				lects_html += X;
			} else {
				lects_html += V;
			}
			lects_html += "</td>";
		}
		lects_html += "</tr><tr>";
		for (var i=lects.length-1, j=0 ; j<tablelen ; j++, i--) {
			lects_html += "<td style='text-align: center; padding: 5px; width: 16%;'>";
			if (lects[i] === "0") {
				lects_html += " ";
			} else {
				lects_html += lects[i];
			}
		}
		lects_html += "</tr></table></center>"; // <a href='http://technion-courses.tk/' target='_blank' id='lecturers_label' class='label label-success'>סמסטרים נוספים</a>
		return lects_html;
	} catch (err) {
		return "שגיאה";
	}
}

function getPastLecturers() {
	chrome.runtime.sendMessage({type: "getPastLecturers", course: courseNumForLects}, function(response) {
		if (response.result === "Cannot contact Technion site.") {
			$("#past_lecturers_line").html("המידע אינו זמין כעת עקב בעיית גישה לשרתי הטכניון");
			return;
		}
		data = JSON.parse(response.result);
		// console.log(data);
		if (data.courseData.exists === true) {
			$("#past_lecturers_line").html(pastLecturersHTML(data.courseData.info.semesters));
		} else {
			$("#past_lecturers_line").html("לא קיים מידע אודות מרצים בשנים קודמות");
		}
	});
}

/* ====================================================================================================== */

var courseTmp, courseNum, rate_data, timeout, page_type=0, groups, globid, globpass;

// In case we're in a search page, we would like to load the remaining seats into the courses in the page
// We first check to see if the user has provided credentials for the UG.
haveMishalCreds(function (res) {
	if (res === 1) {
		addSeatsLeftToSearch();
	}
});

// Done with the search page handling, checking page title to make sure we are in a course page.
if (document.title.indexOf("קורס") != -1) {
	search_page = 0;
	// We use the window pate to fetch the course number. There are 2 types of paths:
	// 1. https://ug3.technion.ac.il/rishum/course/Course_Number
	// 2. https://ug3.technion.ac.il/rishum/course?MK=Course_Number&CATINFO=&SEM=YYYYSS where YYYY=Year and SS=Semester
	/* ====================================================================================================== */
	// We distinguish between the 2 through the following code, and save the course number
	var pathArray = window.location.pathname.split( '/' );
	courseTmp = parseInt(pathArray[3]);
	if (courseTmp) {
		page_type=1;
		courseNum=courseTmp;
	}
	if (!courseTmp) {
		courseTmp = window.location.href.split('=')[1].split('&')[0];
		page_type=2;
		courseNum=parseInt(courseTmp);
	}
	courseNumForLects = courseNum;
	if (courseNum < 100000) {
		courseNumForLects = "0" + courseNum;
	}
	// console.log("courseNum: "+courseNumForLects);
}
// The system does not support Physical Education courses for several reasons,
// so we don't run the script for those courses
if (document.title.indexOf("קורס") != -1 && !(courseNum>=394800 && courseNum<=394903) ) {
	// The code in the ~100 lines below creates the HTML elements we are injecting into the code and injects them into the page.
	// The data is loaded into a Bootstrap modal that has an iframe in it to show the information from our servers.
	// This is done in a very inefficient and ugly javascript - TODO: Change to more concise jQuery instead.
	var review = document.createElement('div');
	var dummy = document.createElement('div');
	dummy.setAttribute('id', 'dummy');
	review.setAttribute('class', 'properties-section');
	review.innerHTML = "חוות דעת סטודנטים";
	var elements = document.getElementsByClassName("properties-wrapper");
	elements[0].insertBefore(dummy, elements[0].firstChild);
	elements[0].insertBefore(review, elements[0].firstChild);

	var field = document.createElement('div');
	var value = document.createElement('div');
	field.setAttribute('class', 'property');
	value.setAttribute('class', 'property-value');
	value.setAttribute('id', 'reviews_line');

	var field1 = document.createElement('div');
	var value1 = document.createElement('div');
	field1.setAttribute('class', 'property');
	value1.setAttribute('class', 'property-value');
	value1.setAttribute('id', 'grades_line');

	var field2 = document.createElement('div');
	var value2 = document.createElement('div');
	field2.setAttribute('class', 'property');
	value2.setAttribute('class', 'property-value');
	value2.setAttribute('id', 'past_lecturers_line');

	field2.innerHTML = "פעילות לפי סמסטרים<br><a href='http://technion-courses.tk/' target='_blank' style='font-size: 12px;'>מידע נוסף</a>";
	value2.innerHTML = "<a href='javascript:void(0)' id='lecturers_label' class='label label-success'>אוסף נתונים...</a>";
	dummy.insertBefore(value2, dummy.firstChild);
	dummy.insertBefore(field2, dummy.firstChild);
	field1.innerHTML = "ציונים";
	value1.innerHTML = "<a href='' id='mod1' class='label label-success'>הצגת היסטוגרמות</a> <span id='grades_data'>אוסף נתונים...</span>";
	dummy.insertBefore(value1, dummy.firstChild);
	dummy.insertBefore(field1, dummy.firstChild);
	field.innerHTML = "חוות דעת סטודנטים";
	value.innerHTML = '<span id="rate_link"></span><div id="stars" class="rateit" style="direction:ltr; display: none;" data-rateit-value="0" data-rateit-resetable="false" data-rateit-readonly="true"></div> <span id="rate_text">אוסף נתונים...</span>';
	dummy.insertBefore(value, dummy.firstChild);
	dummy.insertBefore(field, dummy.firstChild);

	var modal1 = document.createElement('div');
	modal1.setAttribute('style', 'display: none');
	modal1.setAttribute('id', 'modal1');
	modal1.innerHTML = '<div class="modal fade" id="screen1" tabindex="-1" role="dialog" aria-labelledby="Label" aria-hidden="true" style="width: 70%; height: 80%; margin-left: -35%;"/>\
						  <div class="modal-dialog"/>\
						    <div class="modal-content"/>\
						      <div class="modal-header"/>\
						        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"/>&times;</span><span class="sr-only"/></span></button/>\
						        <center>\
						        	<h2>היסטוגרמות וממוצעים</h2>\
						      	</center>\
						      </div/>\
						      <div class="modal-body" style="max-height: 100%;"/>\
						      <center><iframe id="iframe1" src="https://coursesmarts.tk/V2/serve_h.php?q='+courseNum+'" seamless="seamless" width="100%" height="80%" align="right" style="border:none; float:right; overflow-y: hidden; overflow-x: hidden;"></iframe></center>\
						      </div/>\
						    </div/>\
						  </div/>\
						</div/>';

	var modal2 = document.createElement('div');
	modal2.setAttribute('style', 'display: none');
	modal2.setAttribute('id', 'modal2');
	modal2.innerHTML = '<div class="modal fade" id="screen2" tabindex="-1" role="dialog" aria-labelledby="Label" aria-hidden="true" style="width: 70%; height: 80%; margin-left: -35%;"/>\
						  <div class="modal-dialog"/>\
						    <div class="modal-content"/>\
						      <div class="modal-header"/>\
						        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"/>&times;</span><span class="sr-only"/></span></button/>\
									<center/>\
										<h2 id="hb">חוות דעת סטודנטים</h2/>\
									</center>\
						      </div/>\
						      <div class="modal-body" style="max-height: 100%;"/>\
						      	<center><iframe id="iframe2" src="https://coursesmarts.tk/V2/serve.php?q='+courseNum+'" seamless="seamless" width="100%" height="80%" align="right" style="border:none; float:right; overflow-y: hidden;"></iframe></center>\
						      </div/>\
						    </div/>\
						  </div/>\
						</div/>';

	var form_html = '<form id="mishform1">\
					    <div class="form-group" style="margin-top: 20px;">\
						    <input type="tel" class="form-control login-input" name="UID" id="mishid" placeholder="תעודת זהות" style="height: 34px; font-size: 14px; line-height: 1.42857143; border-radius: 4px;">\
						</div>\
						<div class="form-group" style="margin-top: 20px;">\
						    <input type="password" class="form-control login-input" name="PWD" id="mishpass" placeholder="סיסמא לUG" style="height: 34px; font-size: 14px; line-height: 1.42857143; border-radius: 4px;">\
						</div>\
						<div class="form-group" style="margin-top: 20px;">\
						    <button id="mishfrom_submit" class="btn btn-primary">שלח</button><button type="button" class="btn btn-primary" data-dismiss="modal" style="margin-right: 10px;">סגור</button>\
						</div>\
					</form>';
	var modal3 = document.createElement('div');
	modal3.setAttribute('style', 'display: none');
	modal3.setAttribute('id', 'modal3');
	modal3.innerHTML = '<div class="modal fade" id="screen3" tabindex="-1" role="dialog" aria-labelledby="Label" aria-hidden="true">\
						  <div class="modal-dialog">\
						    <div class="modal-content">\
						      <div class="modal-header">\
						        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"/>&times;</span><span class="sr-only"/></span></button/>\
									<center/>\
										<h2 id="hb">התחברות חד פעמית למשאל המרצה</h2>\
										\
									</center>\
						      </div/>\
						      <div class="modal-body">\
						      	כדי שהמערכת תוכל לשלוף עבורך את שקלול הציונים במשאל המרצה והמתרגל, דרושה התחברות חד פעמית.\
						      	נתוני ההתחברות שלך <b><u>אינם</u></b> ישלחו אלינו או ישמרו אצלנו. הנתונים ישמרו אצלך בדפדפן ויעשה בהם שימוש <b><u>רק</u></b> עבור התחברות למשאל המרצה והמתרגל.<br>\
						      	<center><div id="mishform">\
						      	'+form_html+'\
								</div></center><br>\
						      </div/>\
						    </div/>\
						  </div/>\
						</div/>';

	document.body.insertBefore(modal1, document.body.lastChild);
	document.body.insertBefore(modal2, document.body.lastChild);
	document.body.insertBefore(modal3, document.body.lastChild);

	var script = document.createElement("script");
	script.innerHTML = '$( "#mod1" ).click(function( event ) {\
							document.getElementById("iframe1").setAttribute("src", "https://coursesmarts.tk/V2/serve_h.php?q='+courseNum+'");\
							$("#screen1").modal("show");\
							document.getElementById("modal1").setAttribute("style", "display: block");\
							event.preventDefault();\
						});\
						$( "#rate_link" ).click(function( event ) {\
							document.getElementById("iframe2").setAttribute("src", "https://coursesmarts.tk/V2/serve.php?q='+courseNum+'");\
							$("#screen2").modal("show");\
							$("#modal2").attr("style", "display: block");\
							event.preventDefault();\
						});';
	
	document.body.insertBefore(script, document.body.lastChild);
	$(function(){
		$( "#aform" ).click(function( event ) {
			$("#screen3").modal("show");
			document.getElementById("modal3").setAttribute("style", "display: block");
			event.preventDefault();
		});	
	});

	getPastLecturers();

	/* ====================================================================================================== */

	// OK, all of the elemets were injected into the page and it's time to load the students' course rating and the course average.
	getRatings();
	getAverages();

	// =================== MISHAL Connection Form / MISHAL Grades Loader =================== //
	// The code below is responsible to load the lecturers survey data into the page.
	// "lecturers" is a global array the will hold the urls to the survey page.
	var lecturers = [];
	// Check if the user supplied credentials for the UG system so the grades can be fetched.
	// If not, let the user the option to enter his\her credentials.
	// Else, load the grades using loadMishalGrades()
	haveMishalCreds(function (res) {
		if (res === 0){
			groups = $('.container .properties-section:contains("קבוצות רישום")');
			if (groups) {
				groups.html("קבוצות רישום <a href='javascript:void(0)' id='aform' style='position: relative; right: 7%; color:red;'>לחץ כאן להכנסת נתוני משאל המרצה והמתרגל במקום סימני השאלה</a>");
			}
			$( "a[href*='cics']" ).each(function(id){
				$( "<span class='badge badge-success mish_empty' data-toggle='tooltip' data-placement='top' title='התחברו למשאל חד פעמית להצגת השקלול (הקישור ליד קבוצות רישום)' style='margin-right: 5px;' id='lect_"+id+"'>?</span>" ).insertAfter( $(this) );
			});
			$('[data-toggle="tooltip"]').tooltip();
		} else if (res === 1) {
			loadMishalGrades();
			addSeatsLeftToSearch();
			loadSeatsToCoursePage();
		}
	});

	// The code below is resposible for handling the form for entering the UG system's credentials.
	// It is invoked once the user submits the form.
	// It gathers the username and password, and then calls checkMishalCreds() to verify the credentials with the technion's server.
	// If the credentials are correct, it calls loadMishalGrades() to load the grades into the page.
	// Else, we let the user know that username or password are not correct.
	var auth_failed = 0;
	$( "#mishfrom_submit" ).click(function mishclick( event ) {
		event.preventDefault();
		$('<img id="loadgif" src="'+chrome.extension.getURL("/img/loading1.gif")+'" style="margin-top: 15px;">').insertBefore("#mishform1");
		var id = $("#mishid").val();
		var pass = $("#mishpass").val();
		checkMishalCreds(id, pass, function(res) {
			if (res === 1) {
				$("#aform").html('');
				$(".mish_empty").remove();
				loadMishalGrades();
				loadSeatsToCoursePage();
				$("#screen3").modal("hide");					
			} else {
				$("#loadgif").remove();
				if (auth_failed === 0){
					$('<div class="alert alert-danger" style="margin-top: 15px;" role="alert">התעודת זהות ו/או הסיסמא אינם נכונים, ניתן לנסות שוב</div>').insertBefore("#mishform1");
					auth_failed = 1;
				}
			}
		});
	});	

	// Fetch the name of the lecturer in charge this semester.
	// Then, call getLecturerAvg() to retreive the course average when the specific lecturer is in charge.
	var lecturer_in_charge = $( "div:contains('אחראים')" ).next(".property-value").text();
	//console.log(lecturer_in_charge);
	getLecturerAvg(lecturer_in_charge, courseNum);
}

if (document.title.indexOf("קורס") != -1 && (courseNum>=394800 && courseNum<=394903) ) {
	haveMishalCreds(function (res) {
		if (res === 1) {
			loadSeatsToCoursePage();
		}
	});
}

// chrome.storage.sync.remove(["mishalid", "mishalpass"]);

// New version messages
// This mechanism allows us to broadcast a new version release message whenever this happens
// We store an increasing message id, and the message will appear if the saved id is lower than the new one.
// After the message is showed, the id is updated so the message won't show up again.
var heading = "<center>CourseSmarts שודרג!</center>";
var text = "<b>אנחנו שמחים להודיע על עדכון לתוסף! מה חדש?</b><br><ul style='margin-right:15px;'>\
<li><b><u>מקומות פנויים בקורס:</u></b> המקומות הפנויים בקורס או בקבוצה יופיעו ליד הכפתור לבדיקת המקומות הפנויים, הן בחיפוש קורסים במערכת, והן בתוך עמוד הקורס. <u>אזהרה:</u> שימוש מאסיבי בחיפוש (למשל, חיפוש קורס שמניב למעלה מ20 תוצאות) עלול לגרום לחסימה <u>זמנית</u> מהUG שלא תאפשר בדיקת מקומות פנויים במשך כחצי שעה. כדי למנוע חסימה לא מכוונת, לאחר בדיקת המקומות הפנויים ב10 קורסים שונים בחיפוש, תדרשו לאשר את טעינת ה10 הבאים.</li>\
<li>עקב בלבול שנוצר, <b><u>דירוג העומס בקורס עודכן</b></u> ואנו מקווים שכעת הוא ברור יותר: יותר משקולות = עומס רב בקורס, מעט משקולות = עומס קטן בקורס.</li>\
<li>מידע אודות <b><u>פעילות הקורס בסמסטרים קודמים</u></b> (האם נפתח או לא) יחד עם המרצה האחראי בכל סמסטר התווסף לעמוד הקורס.</li>\
<li><b><u>היסטוגרמה וממוצע משוקלל:</u></b> החל מהסמסטר האחרון חל שינוי באפגרייד במשמעות המשימה 'ציון סופי במקצוע', הסבר אודות השינוי וכיצד הוא בא לידי ביטוי בתוסף ניתן למצוא בפוסט בפייסבוק שמצורף.</li>\
<li>מצאתם בעייה לאחר העדכון? נשמח לדעת!</li>\
<li><b>פרטים נוספים על העדכון ניתן למצוא <a href='https://www.facebook.com/CourseSmarts/posts/1709251479404352' target='_blank'>בפוסט שלנו בפייסבוק.</a></b></li>\
</ul>";
latestMessage = 3;

function showMessage() {
	$.toast({
	    text: text, // Text that is to be shown in the toast
	    heading: heading, // Optional heading to be shown on the toast
	    icon: 'info', // Type of toast icon
	    showHideTransition: 'slide', // fade, slide or plain
	    allowToastClose: true, // Boolean value true or false
	    hideAfter: false, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
	    stack: false, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
	    position: 'bottom-left', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
	    textAlign: 'right',  // Text alignment i.e. left, right or center
	    loader: true,  // Whether to show loader or not. True by default
	    loaderBg: '#9EC600',  // Background color of the toast loader
	});
}

chrome.storage.sync.get('messageid', function(items) {
    messageid = items.messageid;
    if (messageid) {
    	if ((parseInt(messageid) < parseInt(latestMessage))) {
        	showMessage();
        	chrome.storage.sync.set({messageid: latestMessage});
		}    
    } else {
    	chrome.storage.sync.set({messageid: 1});
    }
});
