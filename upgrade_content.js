/* UpGrade Content Script - This script only runs on UpGrade pages */

// Before you dive in - YES, some data pulling is hardcoded for the page layout and YES, we know it'll break if the layout changes.
// We collaborate with the UpGrade's programmers, they said they will to let us know in advanced if the layout changes. If you have a smarter idea, we'll be glad to know!
// Since this code strongly relies on the page structure, we advise you to view the relevant page source in parallel to reading this, for better understanding.

/* Function Declaration and Implementation */
/* ====================================================================================================== */
var clicked = 0, processed = 0;
var courseNum = 0, semester, year, sem_end=0;

// Converting a semester name to its number according to the convention mentioned in the README.md
function semName2Num(semester) {
	if (semester === "חורף")
		return 1;
	if (semester === "אביב")
		return 2;
	if (semester === "קיץ")
		return 3;
	return -1;
}

// Small jQuery trick to find out if an element exists on the page
$.fn.exists = function () {
    return this.length !== 0;
};

// A "rating" is the grade a student gave to the course. There are 2 types: 'general' and 'load'.
// This function submits a user rating to the server.
function sendRating(rate_type, rating) {
	chrome.runtime.sendMessage({type: "postRate", course: courseNum, rate_type: rate_type, rating: rating}, function(response) {
		if (response.result === "success") {
				//console.log("success");
			} else {
				console.log(response.result);
			}
	});	
}

// This function updates a user rating on the server. (In case he changes his mind more than once or more and changes his initial rating)
function updateRating(rate_type, rating, old_rating) {
	chrome.runtime.sendMessage({type: "postRate", course: courseNum, rate_type: rate_type, update: "1", rating: rating, old_rating: old_rating}, function(response) {
		if (response.result === "success") {
				//console.log("success");
			} else {
				console.log(response.result);
			}
	});	
}

// This function is initiated after the user clicks on the "Send" button on the review form, in case he didn't write any review at all and just rated the course.
// If the user changed his initial rating, it should be updated.
// Otherwise, just show the "thank you" message and disappear
function checkRatingChangeAndSend(){
	if (last_geRate != general_rating_stars) {
		updateRating("general", last_geRate, general_rating_stars);
	}
	if (last_loRate != load_rating_stars && load_rating_stars!=-1) {
		updateRating("load", last_loRate, load_rating_stars);
	} else if (load_rating_stars === -1) {
		sendRating("load", 0);
	}
	$("#rating").animate({height:'73', width:'350'});
	$("#ContRate").css("display", "none");
	$("#container").html('<div class="alert alert-info" role="alert"><center><b style="font-size: x-large">הדירוג התקבל, תודה רבה!</b></center></div>');
	setTimeout(function(){
		$("#rating").animate({height:'0'});
		$("#container").css("display", "none");
		setTimeout(function(){$("#rating").css("display","none");}, 400);
	}, 4000);
}

// This function sends average and median for a specific moed to the server
// Since each course has its own page on upgarde, the course number, semester and year are global throughout this script.
// Thats why there's no need to send them as arguments as well.
function postAverages(average, median, moed) {
	console.log("Avg: "+average+" Med: "+median);
	chrome.runtime.sendMessage({type: "postAvg", course: courseNum, semester: semester, year: year, averages: average, medians: median, part: '1', moed: moed}, function(response) {
		console.log(response.result);
	});
}

// The postHistogram wrapper function calls this function once the histogram picture is loaded into the page.
// This function uses canvas and an imported library in order to fetch the histogram from the page. (like screen capture, just for the histogram's div)
// We do this the dirty way because we didn't succeed with accessing the image directly. If you know how, please let us know!
// Then we send both the histogram and the relevant data below it to the server.
// The try/catch is here to catch changes in the layout of the page before they harm our data by sending wrong data to the server.
// Note: before "taking the picture" of the histogram we hide the 'rank' of the student in the class since it is personal, nevertheless:
// There's no way to indentify the student who uploaded the histogram anyway, even for us!
function postHistogram(moed) {
	elements = document.getElementsByClassName("jsPanel-content");
	elements[0].setAttribute('id', 'histo');
	var image = document.getElementById("CourseChart");
	var table = document.getElementById("gvChart");
	var rank = $("#gvChart tbody tr td:eq(6)").text();
	$("#gvChart tbody tr td:eq(6)").text(' ');
	html2canvas(table, {
	  onrendered: function(t_canvas) {
	  	var canvas = document.getElementById("histCanv");
	    var ctx = canvas.getContext('2d');
	    canvas.width = image.width;
	    canvas.height = image.height + t_canvas.height;
	    ctx.drawImage(image, 0, 0);
	    ctx.drawImage(t_canvas, (image.width-t_canvas.width)/2, image.height);
	    var dataURL = canvas.toDataURL("image/png");
	    chrome.runtime.sendMessage({type: "postHist", course: courseNum, semester: semester, year: year, moed: moed, dataURL: dataURL, webc: '0', last_exam: last_exam}, function(response) {
	    	console.log(response.result);
	    	$("#gvChart tbody tr td:eq(6)").text(rank);
	    });
	  }
	});

	try {
		if (moed === "A" || moed === "B" || moed == "total"){
			var avg_idx, med_idx;
			$("#gvChart").find("th").each(function($index){
				if ($(this).text() == "Average") {
					avg_idx = $index;
				}
				if ($(this).text() == "Median") {
					med_idx = $index;
				}
			});
			console.log("AvgIdx:"+avg_idx+"MedIdx:"+med_idx);
			if (typeof avg_idx == 'undefined' || typeof med_idx == 'undefined' || avg_idx < 0 || med_idx < 0) {
				throw {message: "Upgrade: Can't find the indexes of the avg and median in general table", lineNumber: "126"};
			}
			var average = $("#gvChart tbody tr td:eq("+avg_idx+")").text();
			var median = $("#gvChart tbody tr td:eq("+med_idx+")").text();
			// console.log("Ave:"+average+"Med:"+median);
			if (average>100 || average<=0 || median>100 || median <= 0) {
				throw {message: "Grades Submitted in "+courseNum+", "+year+", "+semester+", "+moed+" are illegal! average="+average+", median="+median, lineNumber: "132"};
			}
			postAverages(average, median, moed);
		}
	} catch(error) {
		chrome.runtime.sendMessage({type: "reportErr", message: error.lineNumber+":"+error.message}, function(response) {
			console.log(response.result);
		});	
	}
	
}

// Wrapper function for the function that is resposible for uploading the histogram to the server.
// We just wait until the image is loaded and then call the postHistogram function.
function postHistgramWrap(moed) {
	$("#CourseChart").one("load", function() {
	  postHistogram(moed);
	}).each(function() {
	  if(this.complete) $(this).load();
	});
}
/* ====================================================================================================== */

// Fetching the course number, semester, year and username from one of the headers
try {
	courseNum = $.trim($("#headerLbl").text().split(' ')[0]);
	var html_content = $("#headerLbl").html();
	html_content = html_content.split('<br>')[1];
	semester = html_content.split(' ')[1];
	semester = semName2Num(semester);
	year = html_content.split(' ')[2];
	year = parseInt(year.split('/')[1]) + 2000; // Yep, it won't work in the next millenium. OMG BUG 3000!
	var u_num = $("#LabelStudent").html().split(' ').length;
	var username = $("#LabelStudent").html().split(' ')[u_num-2];
} catch(error) {
	chrome.runtime.sendMessage({type: "reportErr", message: error.lineNumber+":"+error.message+" URL: "+window.location.href}, function(response) {
		console.log(response.result);
	});	
}

// In the code below we inject the (currently invisible) elements we need into the page.
/* ====================================================================================================== */
// This is an invisible modal that was mainly used for debugging the histogram fetching.
// The Canvas inside (id="histCanv") is where we build the picture that will later be uploaded.
// (If we make the modal visible we can see what will be uploaded, but we don't need that for regular use) 
var modal1 = document.createElement('div');
modal1.setAttribute('id', 'modal1');
modal1.innerHTML = '<div class="modal fade" id="screen1" tabindex="-1" role="dialog" aria-labelledby="Label" aria-hidden="true" style="z-index: 100000;"/>\
					  <div class="modal-dialog"/>\
					    <div class="modal-content"/>\
					      <div class="modal-header"/>\
					        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"/>&times;</span><span class="sr-only"/></span></button>\
					      </div/>\
					      <div class="modal-body" id="modal_body" style="height: 100%;"/>\
					      <canvas id="histCanv" class="img-responsive" style="border:3px solid black"></canvas>\
					      </div/>\
					    </div/>\
					  </div/>\
					</div/>';

document.body.insertBefore(modal1, document.body.lastChild);

var rating = document.createElement('div');
rating.setAttribute('id', 'rating');
rating.innerHTML = '<button type="button" id="close_rating" class="close" style="float: right; margin-right: 10px;"><span aria-hidden="true"/>&times;</span><span class="sr-only"/></span></button><div id="container"><center><b><p style="font-size: x-large; margin-top: 10px; margin-left: 20px;">'+username+', רוצה לדרג את הקורס?</p><p style="font-size: large; display: inline;">דירוג כללי: </p></b>\
					<img src="'+chrome.extension.getURL("/img/happy.png")+'" style="margin-bottom: 15px; display: inline;"><div id="rating_stars" class="rateit bigstars" data-rateit-resetable="false" data-rateit-starwidth="32" data-rateit-starheight="32" style="direction: ltr; display: inline;"></div><img src="'+chrome.extension.getURL("/img/unhappy.png")+'" style="margin-bottom: 15px; display: inline;"></div>\
					<div id="ContRate" style="display: none; text-align: center;"><center><b><p style="font-size: large; display: inline;">דירוג עומס: </p></b>\
					<img src="'+chrome.extension.getURL("/img/unhappy.png")+'" style="margin-bottom: 15px; display: inline;"><div id="load_stars" class="rateit bigload" data-rateit-resetable="false" data-rateit-starwidth="32" data-rateit-starheight="32" style="padding-top: 15px; direction: ltr; display: inline;"></div><img src="'+chrome.extension.getURL("/img/happy.png")+'" style="margin-bottom: 15px; display: inline;"><br>\
					<p style="font-size: large;">חוות דעת כללית:</p>\
					<textarea name="general" id="general" style="overflow:hidden; width:530px;" data-toggle="tooltip" data-placement="bottom" title="לא ניתן לשלוח חוות דעת ריקה" class="form-control" rows="3" placeholder="התייחסות כללית לגבי הקורס, האם לדעתך הקורס היה מעניין, האם רלוונטי, האם בדיעבד היה שווה להרשם לקורס? האם יש הבדל בקורס בסמסטרים שונים? האם המרצה שהעביר את הקורס מומלץ לדעתך?"></textarea><br>\
					<p style="font-size: large;">חוות דעת עומס:</p>\
					<textarea name="load" id="load" style="overflow:hidden; width:530px;" class="form-control" rows="3" placeholder="נקודות שכדאי להתייחס אליהן בנושא העומס: כמות תרגילי בית במהלך הסמסטר, (רטובים ויבשים) כמה זמן דרוש להשקיע בתרגילי בית, כמה זמן ביום/שבוע צריך להשקיע בשביל המטלות בקורס זה באופן כללי."></textarea><br>\
					<span>*שמך הפרטי יופיע יחד עם חוות הדעת</span><br>\
					<input id="anon_chckbox" type="checkbox" name="anon_user" value="אנונימי"><span style="color: black; font-size: small">פרסום אנונימי</span><br><br>\
					<a id="star_send" class="btn btn-primary" style="text-align: center; width: 80px; margin-top: 15px; display: inline;">שלח וסיים</a><a id="star_close" class="btn btn-danger" style="text-align: center; width: 50px; display: inline; margin-right: 15px">סגור</a>\
					</div></center>';
document.body.insertBefore(rating, document.body.lastChild);
$("#rating").css({"border-width":"3px","border-style":"solid","position":"fixed","top":"20px","left":"20px","width":"350px","height":"110px","display":"none","border-radius":"15px", "border-color":"#09575D", "background":"rgba(255,255,255,0.95)", "z-index":"100000"});
$("#rating").attr("dir", "rtl");

/* ====================================================================================================== */

var general_rating_stars, load_rating_stars=-1, last_loRate, last_geRate;
var geRated=0, loRated=0;

// Bind handling functions to the stars rating. Once the user clicks on them, the rating is sent immedietly
// This is built this way to generate more ratings. The user doesn't have to click send to post his ratings
// This is not the case for reviews in which the user must click send to publish.
$("#rating_stars").bind('rated', function() { 
						if (!geRated) {
							general_rating_stars=$(this).rateit('value');
							$("#rating").animate({height:'510', width:'550'});
							$("#ContRate").css("display", "block");
							sendRating("general", general_rating_stars);
							geRated=1;
						}
					});
$("#load_stars").bind('rated', function() { 
						if (!loRated) {
							load_rating_stars=$(this).rateit('value');
							sendRating("load", load_rating_stars);
							loRated=1;
						}
					});

$("#close_rating").click(function(){
	$("#rating").animate({height:'0'});
	$("#container").css("display", "none");
	$("#ContRate").css("display", "none");
	setTimeout(function(){$("#rating").css("display","none");}, 400);	
});

$("#star_send").click(function() {
	last_geRate = $("#rating_stars").rateit('value');
	last_loRate = $("#load_stars").rateit('value');

	if (!$.trim($("#general").val()) && !$.trim($("#load").val())) {
    	checkRatingChangeAndSend();
    	return;
	}
	var general = $("#general").val();
	var load = $("#load").val();

	if ($("#anon_chckbox").prop("checked") == true){
		username = "אנונימי";
	}

	chrome.runtime.sendMessage({type: "postRev", course: courseNum, semester: semester, year: year, general: general, load: load, username: username}, function(response) {
		if (response.result != "success") {
			//console.log(response.result);
		}
	});
	checkRatingChangeAndSend();
});

$("#star_close").click(function() {
	$("#rating").animate({height:'0'});
	$("#ContRate").css("display", "none");
	setTimeout(function(){
		$("#rating").css("display", "none");
	}, 300);
});
/* ====================================================================================================== */

// The loop below iterates through the main table in the course page in the upgrade system
// It has 2 main purposes:
// 1. Bind a wrapper function to the histogram button that will be called once the button is pressed.
// This function is resposible for capturing and uploading the histogram to the server.
// 2. Determine if the semester didn't end (no final grade), and in case it has ended,
// to determine the last final grade available. (either moed A or both moed A and moed B)
// Why do we need to know this?
// The task "Final" or "ציון סופי במקצוע" represents the grades as they appear in the UG system, thus, this is the most up-to-date and reliable data.
// Once we know if only moed A took place or both moed A and moed B, we know how updated is the task "Final".
// This is the only way (at the moment) to maintain an average of both moed A and moed B toghether.
// The try/catch is there to try and catch changes in the page layout before they harm our data by sending wrong data to the server.
try {
	var mainTableId = "CBody_GridView_StudentGrade";
	var last_exam;
	if (!$("#"+mainTableId+" tbody tr").exists()) {
		throw {message: "Upgrade Main Table not found! Course:"+courseNum+" Sem:"+semester+" Year:"+year+" Path: "+window.location.href, lineNumber: "291"};
	}
	$("#"+mainTableId+" tbody tr").each(function(tr_index){
		$(this).children().each(function(td_index){
			if ($(this).text() === "ציון סופי במקצוע" && $(this).next().text() === "Finals") {
				sem_end=1;
				$("#"+mainTableId+" tbody tr:eq("+tr_index+")").last().first().click(function(){
					setTimeout(postHistgramWrap, 1500, "total");
				});
			}
			if ($(this).text() === "Final_A" || $(this).text() === "Final_B"){
				sem_end=1;
				var moed = $(this).text().split('_')[1];
				if ($(this).text() === "Final_A" && last_exam != "B") {
					last_exam = moed;
				}
				if ($(this).text() === "Final_B") {
					last_exam = moed;
				}
				if (typeof last_exam == 'undefined' && sem_end == 1) {
					last_exam = "A";
				}
				$("#"+mainTableId+" tbody tr:eq("+tr_index+")").last().first().click(function(){
					setTimeout(postHistgramWrap, 1500, moed);
				});
				return false;
			}

		});
	});
	console.log(last_exam);

	// This loop iterates through the second table in the upgarde's course page.
	// It has the same objectives of the loop above, except for the fact it only looks for the "Finals" task and takes the average and median straight away.
	var total_average, total_median;
	var i;
	for (i=0 ; i<3 ; i++) {
		if ($("#gv tbody tr:eq("+i+") td:eq(3)").text() === "Finals" && ($("#gv tbody tr:eq("+i+") td:eq(2)").text() === "Main_Computer(grades)" || $("#gv tbody tr:eq("+i+") td:eq(2)").text() === "ציון סופי במקצוע") ) {
			var avg_idx, med_idx;
			$("#gv").find("th").each(function($index){
				if ($(this).text() == 'Average') {
					// console.log($index);
					avg_idx = $index;
				}
				if ($(this).text() == 'Median') {
					med_idx  = $index;
				}
			});
			if (typeof avg_idx == 'undefined' || typeof med_idx == 'undefined' || avg_idx < 0 || med_idx < 0) {
				throw {message: "Upgrade: Can't find the indexes of the avg and median in general table", lineNumber: "340"};
			}
			console.log("AvgIdx:"+avg_idx+"MedIdx:"+med_idx);
			total_average = $("#gv tbody tr:eq("+i+") td:eq("+avg_idx+")").text();
			total_median = $("#gv tbody tr:eq("+i+") td:eq("+med_idx+")").text();
			$("#gv tbody tr:eq("+i+") td:eq(12)").first().click(function(){
		 		setTimeout(postHistgramWrap, 1500, "total");
		 	});
			if (total_average>100 || total_average<=0 || total_median>100 || total_median <= 0) {
				throw {message: "Grades Submitted in "+courseNum+", "+year+", "+semester+", total are illegal! total_average="+total_average+", total_median="+total_median, lineNumber: "349"};
			}
		 	postAverages(total_average, total_median, "total");
		 	break;
		}
	}	
} catch(error) {
	// This message lets us know there is something wrong with the JS execution.
	chrome.runtime.sendMessage({type: "reportErr", message: error.lineNumber+":"+error.message}, function(response) {
		console.log(response.result);
	});	
}

// Last thing - the review form was injected, but it's invisible to the user.
// We first check to see if the user has already submitted a review to this course.
// If so, no need to show him anything. Otherwise, show the review request form.
chrome.runtime.sendMessage({type: "identify", course: courseNum, name: username}, function(response) {
	response = response.result.split(' ');
	if (response[0] === '0' && sem_end) {
		setTimeout(function(){
			$("#rating").slideDown("slow");
		}, 2000);
	}
});
