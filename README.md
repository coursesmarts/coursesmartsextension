# CourseSmarts #
## What is CourseSmarts? ##
* CourseSmarts is a chrome extension that was designed and built by Technion students.
* CourseSmarts is about making a collaborative effort for outsmarting course taking decisions.
* CourseSmarts is a community based tool for uploading course reviews, grade averages and grade histograms for the Technion.
* CourseSmarts is built upon the Technion's course registration and grades distribution  systems. The extension collects information (Non-personal information only) automatically when the user is viewing the course page in both the UpGrade system and the WebCourse system used by CS faculty.
* CourseSmarts then makes all the information uploaded accessible from the course page in the course registration system.

![screenshot.png](https://bitbucket.org/repo/5Bko5M/images/2528236139-screenshot.png)

We hope this repository will hold the information needed to maintain the extension once our lovely time at the Technion will come to an end.
If you feel there are missing pieces - by all means let us know and we'll do our best to improve the documentation.

### We're saying it upfront: ###
We are not javascript/jquery ninjas. In fact, for the most part this is the first experience we have with these tools, and some of the code was written in a quick & dirty manner. We are aware of that, and we'll make improvements with time.

### Technicalities ###
Due to the fact that cross origin requests are not allowed on the Technion's web server, we needed to make the requests in a different context.
Luckily, the chrome extension allows to make requests in the extension's context on behalf of the content script. (If you are not familiar with these terms, you should read through some basic Chrome Extension documentation, it's pretty simple) Changing context is achieved by using a messaging mechanism between the background script which runs in the extension's context and the content script which runs in the Technion's webpage. The messaging mechanism is implemented by Google as a part of the browser extension's interface.

### Conventions ###
1. Semester Numbers: Semesters are represented by number in both the UG and the extension - Winter = 1, Spring = 2, Winter = 3.

2. Year and Semester: Throughout the extension, winter semester which starts at the end of a year X and ends at year X+1 will be called "Winter X+1". For example, the semester that started on October 2015 and ended on February 2016 will be called in the extension "Winter 2016". This is somewhat different from the convention on some of the Technion's systems, but we decided to do it this way for simplicity. (Each semester is represented by one name and one year) Spring and Summer semesters do not need special treatment.