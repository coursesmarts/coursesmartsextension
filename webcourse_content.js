/* WebCourse Content Script - This script only runs on webcourse pages */

// Before you dive in - YES, some data pulling is hardcoded for the page layout and YES, we know it'll break if the layout changes.
// If you have a smarter idea, we'll be glad to know!
// Since this code strongly relies on the page structure, we advise you to view the relevant page source in parallel to reading this, for better understanding.

/* Function Declaration and Implementation */
/* ====================================================================================================== */
// Small jQuery trick to find out if an element exists on the page
$.fn.exists = function () {
    return this.length !== 0;
};

// Converting the semester name into its number in the system. Supports the 3 languages in WebCourse
function semName2Num(semester) {
	if (semester === "חורף" || semester === "Winter" || semester === "Зима" || semester==="شتاء")
		return 1;
	if (semester === "אביב" || semester==="Spring" || semester === "Весна" || semester==="ربيع")
		return 2;
	if (semester === "קיץ" || semester==="Summer" || semester === "Лето" || semester==="صيف")
		return 3;
	return -1;
}

function fetchSemAndYear() {
	// Determine which semester is it.
	var data = $("span.semester").text().split(' ');
	semester = semName2Num(data[0]);
	// Check if this is winter semester. See the conventions notes in the readme file to understand why the next piece of code is necessary
	if (semester == 1) {
		year = parseInt(data[1].split('-')[1]);
	} else {
		year = parseInt(data[1]);
	}
	return {semester: semester, year: year};
}

// This function is used for scraping the histograms on the unified grades page on webcourse (the page in which you can see all the grades of a semester, not just for one course).
// Since CS faculty histograms are now also available in the UpGrade system which is far more informative and easy to use,
// this feature is removed, but we've decided to keep the code in case something changes in the future.
// function nextChar(c) {
//     return String.fromCharCode(c.charCodeAt(0) + 1);
// }

// A "rating" is the grade a student gave to the course. There are 2 types: 'general' and 'load'.
// This function submits a user rating to the server.
function sendRating(rate_type, rating) {
	chrome.runtime.sendMessage({type: "postRate", course: courseNum, rate_type: rate_type, rating: rating}, function(response) {
		if (response.result === "success") {
				//console.log("success");
			} else {
				console.log(response.result);
			}
	});	
}
// This function updates a user rating on the server. (In case he changes his mind more than once or more and changes his initial rating)
function updateRating(rate_type, rating, old_rating) {
	chrome.runtime.sendMessage({type: "postRate", course: courseNum, rate_type: rate_type, update: "1", rating: rating, old_rating: old_rating}, function(response) {
		if (response.result === "success") {
				//console.log("success");
			} else {
				console.log(response.result);
			}
	});	
}
// This function is initiated after the user clicks on the "Send" button on the review form, in case he didn't write any review at all and just rated the course.
// If the user changed his initial rating, it should be updated.
// Otherwise, just show the "thank you" message and disappear
function checkRatingChangeAndSend(){
	if (last_geRate != general_rating_stars) {
		updateRating("general", last_geRate, general_rating_stars);
	}
	if (last_loRate != load_rating_stars && load_rating_stars!=-1) {
		updateRating("load", last_loRate, load_rating_stars);
	} else if (load_rating_stars === -1) {
		sendRating("load", 0);
	}
	$("#rating").animate({height:'60', width:'350'});
	$("#ContRate").css("display", "none");
	$("#container").html('<div class="alert-box ab_success"><center><b><span>תודה רבה! </span>הדירוג שלך נשלח</b></center></div>');
	setTimeout(function(){
		$("#rating").animate({height:'0'});
		$("#container").css("display", "none");
		setTimeout(function(){$("#rating").css("display","none");}, 400);
	}, 4000);
}
/* ====================================================================================================== */

var A_table_num, A_td_num, B_table_num, B_td_num, finala_avg, course_page=0;
var courseNum, semester, year, sem_end=0;
//var hist_char = 'b';

// Pulling the student's name
var username = $("span.yellow-text").text();

// At this point, there are two types of pages we care about.
// 1. The page with all the grades of all the courses taken in a specific semester
// 2. The page with grades of a specific course.
// The 2 "if" clauses below determine which page is it by checking if a unique element that only exists in them is present.

/* 1. The page with all the grades of all the courses taken in a specific semester */
/* ====================================================================================================== */
if ($("span.big").exists()) {
	// Fetch semester and year
	sem_year = fetchSemAndYear();
	semester = sem_year.semester;
	year = sem_year.year;

	// The following code iterates through each course present in the page
	$("span.big").each(function (c_span) {
		// Pull the course number
		var course = parseInt($(this).text().split(' ')[0]);
		A_td_num = -1;
		B_td_num = -1;

		//var histo=0;
		// Check to see if a histogram exists. Feature was removed, see comment in the beginning.
		// $(this).nextUntil("span").each(function (sib) {
		// 	var img = $(this).children().first().attr("src");
		// 	if (img != null) {
		// 		img = img.split('/');
		// 		img = img[3];
		// 		if (img === "histogram.gif")
		// 			histo=1;
		// 	}
		// });
		// if (histo) {
		// 	//console.log("finals"+hist_char);
		// 	var histo_link = $("#finals"+hist_char+" img").attr("src");
		// 	//console.log(histo_link);
		// 		chrome.runtime.sendMessage({type: "postHist", course: course, semester: semester, year: year, moed: "total", dataURL: histo_link, webc: '1'}, function(response) {
  		//			console.log("hist: "+course+response.result);
  		//   		});
		// 		hist_char = nextChar(hist_char);
		// }


		// Iterate through elements to find the grades table, don't go beyond this course area. (until next span)
		$(this).nextUntil("span").each(function (id) {
			if ($(this).is("table")){
				// Found a table, now check if a final grade is already there
				$(this).find('em').each(function (index) {
					if ($(this).text() === "finala" || $(this).text() === "final_a" || $(this).text() === "final"){
						// Yes! found a final grade. Indicate the semester has ended, and record the place where "td" in the table where the grade is
						sem_end = 1;
						A_td_num = index+1;
					}
					// Same for Moed b
					if ($(this).text() === "finalb" || $(this).text() === "final_b"){
						B_td_num = index+1;
					}
				});
				// Check if a final grade was found
				if (A_td_num!=-1) {
					// Record the final average.
					var finala = $(this).find('tr').eq(2).find('td').eq(A_td_num).text();
					// Check that a grade was fetched.
					if (finala != "") {
						console.log("post: course: "+course+" semester: "+semester+" year: "+year+" avg: "+finala+" moed: A");
						chrome.runtime.sendMessage({type: "postAvg", course: course, semester: semester, year: year, averages: finala, medians: '0', part: '1', moed: 'A'}, function(response) {
							console.log("response:"+course+response.result);
						});	
					}
					// Change the state to "not found" for next iterations
					A_td_num=-1;
				}
				// Same thing for moed B
				if (B_td_num!=-1) {
					var finalb = $(this).find('tr').eq(2).find('td').eq(B_td_num).text();
					if (finalb != "") {
						console.log("post: course: "+course+" semester: "+semester+" year: "+year+" avg: "+finalb+" moed: B");
						chrome.runtime.sendMessage({type: "postAvg", course: course, semester: semester, year: year, averages: finalb, medians: '0', part: '1', moed: 'B'}, function(response) {
							console.log("response:"+course+response.result);
						});	
					}
					B_td_num=-1;
				}
			}
		});
	});
	// If we are here this is not a course page, so remember that
	course_page = 0;
}
/* ====================================================================================================== */

/* 2. The page with grades of a specific course */
/* ====================================================================================================== */
if ($("td.gradehw").exists() && !$("span.big").exists()){
	// This is the course's grades page
	// Indicate this is a course page, so we can ask the user to submit a review if the semester has ended later on
	course_page = 1;
	// Fetch semester and year
	sem_year = fetchSemAndYear();
	semester = sem_year.semester;
	year = sem_year.year;
	// Fetch the course's number, and handle some corner cases in webcourse
	courseNum = parseInt($("span.titlebarname > span").text().split('-')[0]);
	if (isNaN(courseNum)) {
		courseNum = parseInt( $("span.titlebarname > span").attr("data-lang-he").split('-')[0] );
	}

	// Fetch the table with the grades in it
	$("div.text-content table").each(function (table_id){
		// Iterate to find if a final grade exists
		$(this).find('em').each(function (index) {
			if ($(this).text() === "finala" || $(this).text() === "final_a" || $(this).text() === "final"){
				// Final grade exists, indicate the semester has ended and record the place
				sem_end = 1;
				A_table_num = table_id;
				A_td_num = index+1;
			}
		});		
	});
	// Fetch the final average using the place we just recorded
	finala_avg = $("div.text-content table").eq(A_table_num).find('tr').eq(2).find('td').eq(A_td_num).text();

	// Same thing for moed B
	$("div.text-content table").each(function (table_id){
		$(this).find('em').each(function (index) {
			if ($(this).text() === "finalb" || $(this).text() === "final_b"){
				B_table_num = table_id;
				B_td_num = index+1;
			}
		});		
	});
	// Fetch the final average using the place we just recorded
	finalb_avg = $("div.text-content table").eq(B_table_num).find('tr').eq(2).find('td').eq(B_td_num).text();

	// Check if final moed A and moed B averages were found. If so, send them to the server
	if (finala_avg !== "" && !isNaN(parseFloat(finala_avg)) && !isNaN(courseNum)) {
		console.log("post: course:"+courseNum+"semester:"+semester+"year:"+year+"avg:"+finala_avg+"moed:A");
		chrome.runtime.sendMessage({type: "postAvg", course: courseNum, semester: semester, year: year, averages: finala_avg, medians: '0', part: '1', moed: 'A'}, function(response) {
			console.log("response:"+response.result);
		});		
	}
	if (finalb_avg !== "" && !isNaN(parseFloat(finalb_avg))) {
		console.log("post: course:"+courseNum+"semester:"+semester+"year:"+year+"avg:"+finalb_avg+"moed:B");
		chrome.runtime.sendMessage({type: "postAvg", course: courseNum, semester: semester, year: year, averages: finalb_avg, medians: '0', part: '1', moed: 'B'}, function(response) {
			console.log("response:"+response.result);
		});		
	}
}
/* ====================================================================================================== */

// Finished with handling the grades.
// If there's a final grade (and the semester has ended) and we are on a course grades page, inject the review form to the page
if (sem_end && course_page){
	// I am CERTAIN there is a neater way of doing what I'm doing below, but I don't exactly know what it is
	// Creating an element and injecting it to the page
	var rating = document.createElement('div');
	rating.setAttribute('id', 'rating');
	rating.innerHTML = '<!-- <button type="button" id="close_rating" class="close" style="float: right; margin-right: 10px;">--><span id="close_rating" style="float: right; margin-right: 10px; cursor: pointer;"><img src="'+chrome.extension.getURL("/img/x.png")+'" style="height: 20px; width: 20px;"></span><div id="container"><b><center><p style="font-size: x-large; margin-top: 10px; margin-left: 20px; margin-bottom: -15px; color: black;">'+username+', רוצה לדרג את הקורס?</p><p style="font-size: large; color: black;">דירוג כללי: </p></center></b>\
						<center style="margin-top: -15px;"><img src="'+chrome.extension.getURL("/img/happy.png")+'" style="display: inline;"><div id="rating_stars" class="rateit bigstars" data-rateit-resetable="false" data-rateit-starwidth="32" data-rateit-starheight="32" style="direction: ltr; display: inline;"></div><img src="'+chrome.extension.getURL("/img/unhappy.png")+'" style="display: inline;"></div>\
						<center><div id="ContRate" style="display: none; text-align: center;"><center><b><p style="font-size: large; color: black; margin-bottom: -15px;">דירוג עומס: </p></b></center><br>\
						<img src="'+chrome.extension.getURL("/img/unhappy.png")+'" style="display: inline;"><div id="load_stars" class="rateit bigload" data-rateit-resetable="false" data-rateit-starwidth="32" data-rateit-starheight="32" style="padding-top: 15px; direction: ltr; display: inline;"></div><img src="'+chrome.extension.getURL("/img/happy.png")+'" style="display: inline;"><br>\
						<p style="font-size: large; color: black;">חוות דעת כללית:</p>\
						<textarea name="general" id="general" style="overflow:hidden; width:530px;" data-toggle="tooltip" data-placement="bottom" title="לא ניתן לשלוח חוות דעת ריקה" class="form-control" rows="3" placeholder="התייחסות כללית לגבי הקורס, האם לדעתך הקורס היה מעניין, האם רלוונטי, האם בדיעבד היה שווה להרשם לקורס? האם יש הבדל בקורס בסמסטרים שונים? האם המרצה שהעביר את הקורס מומלץ לדעתך?"></textarea><br>\
						<p style="font-size: large; color: black;">חוות דעת עומס:</p>\
						<textarea name="load" id="load" style="overflow:hidden; width:530px;" class="form-control" rows="3" placeholder="נקודות שכדאי להתייחס אליהן בנושא העומס: כמות תרגילי בית במהלך הסמסטר, (רטובים ויבשים) כמה זמן דרוש להשקיע בתרגילי בית, כמה זמן ביום/שבוע צריך להשקיע בשביל המטלות בקורס זה באופן כללי."></textarea><br>\
						<span style="color: black;">*שמך הפרטי יופיע יחד עם חוות הדעת</span><br>\
						<input id="anon_chckbox" type="checkbox" name="anon_user" value="אנונימי"><span style="color: black; font-size: small">פרסום אנונימי</span><br><br>\
						<a id="star_send" class="btnok" style="text-align: center; width: 80px; margin-top: 15px; display: inline;">שלח וסיים</a><a id="star_close" class="btncancel" style="text-align: center; width: 50px; display: inline; margin-right: 15px">סגור</a>\
						</div></center>';
	document.body.insertBefore(rating, document.body.lastChild);
	$("#rating").css({"border-width":"3px","border-style":"solid","position":"fixed","top":"0","left":"0","width":"350px","height":"110px","display":"none","border-radius":"15px", "border-color":"#006bff", "background":"rgba(255,255,255,0.95)", "z-index":"100000"});
	$("#rating").attr("dir", "rtl");
	var general_rating_stars, load_rating_stars=-1, last_loRate, last_geRate;
	var geRated=0, loRated=0;

	// Bind handling functions to the stars rating. Once the user clicks on them, the rating is sent immedietly
	// This is built this way to generate more ratings. The user doesn't have to click send to post his ratings
	// This is not the case for reviews in which the user must click send to publish.
	$("#rating_stars").bind('rated', function() { 
							if (!geRated) {
								general_rating_stars=$(this).rateit('value');
								$("#rating").animate({height:'480', width:'550'});
								$("#ContRate").css("display", "block");
								sendRating("general", general_rating_stars);
								geRated=1;
							}
						});
	$("#load_stars").bind('rated', function() { 
							if (!loRated) {
								load_rating_stars=$(this).rateit('value');
								sendRating("load", load_rating_stars);
								loRated=1;
							}
						});

	function close_modal() {
		$("#rating").animate({height:'0'});
		$("#container").css("display", "none");
		$("#ContRate").css("display", "none");
		setTimeout(function(){$("#rating").css("display","none");}, 400);			
	}

	$("#close_rating").click(close_modal);

	$("#star_close").click(close_modal);	

	// This function is called when the user clicks send.
	// If there's only rating and not a written review we update the rating
	// Otherwise, there's a review to submit. If the user asked to be anonymous we change his username before submitting
	$("#star_send").click(function() {
						last_geRate = $("#rating_stars").rateit('value');
						last_loRate = $("#load_stars").rateit('value');

						if (!$.trim($("#general").val()) && !$.trim($("#load").val())) {
					    	checkRatingChangeAndSend();
					    	return;
						}
						var general = $("#general").val();
						var load = $("#load").val();

						if ($("#anon_chckbox").prop("checked") == true){
							username = "אנונימי";
						}

						chrome.runtime.sendMessage({type: "postRev", course: courseNum, semester: semester, year: year, general: general, load: load, username: username}, function(response) {
							if (response.result != "success") {
								console.log(response.result);
							}
						});
						checkRatingChangeAndSend();
					});
}

// Last thing - the review form was injected, but it's invisible to the user.
// We first check to see if the user has already submitted a review to this course
// If so, no need to show him anything. Otherwise, show the review request form.
chrome.runtime.sendMessage({type: "identify", course: courseNum, name: username}, function(response) {
	response = response.result.split(' ');
	if (response[0] === '0' && sem_end && course_page) {
		setTimeout(function(){
			$("#rating").slideDown("slow");
		}, 1500);
	}
});
