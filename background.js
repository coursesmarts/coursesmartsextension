
// The content of the messages that the server returns to the background script and then to the content script is formatted in a super duper
// quick & dirty way. We started using JSON instead, and it's in the TDL to change the others.

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

var userid;
var user_data;

function getRandomToken() {
    var randomPool = new Uint8Array(32);
    crypto.getRandomValues(randomPool);
    var hex = '';
    for (var i = 0; i < randomPool.length; ++i) {
        hex += randomPool[i].toString(16);
    }
    return hex;
}

chrome.storage.sync.get('userid', function(items) {
    userid = items.userid;
    if (userid) {
        useToken(userid);
    } else {
        userid = getRandomToken();
        chrome.storage.sync.set({userid: userid}, function() {
            useToken(userid);
        });
    }
    function useToken(userid) {
        identifyUser({type: 'first', course: 1, name: 'none'}, 0);
    }
});

function identifyUser(request, sendResponse){
    var data = "userid="+userid+"&course="+request.course+"&name="+request.name;
    var hr = new XMLHttpRequest();
    hr.open("POST", "https://coursesmarts.tk/V2/identify.php", true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            user_data = hr.responseText;
            if (request.type === "identify"){
                sendResponse({result: user_data});
            }
        }
    }
    hr.send(data);
}

function sendRequest(data, url, sendResponse){
    var hr = new XMLHttpRequest();
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            sendResponse({result: return_data});
        }
    }
    hr.send(data);    
}

function getPastLecturers(request, sendResponse){
    var data="course="+request.course;
    sendRequest(data, "https://coursesmarts.tk/V2/get_past_lects.php", sendResponse);
}

function postHistogram(request, sendResponse){
        var data = "course="+request.course+"&semester="+request.semester+"&year="+request.year+"&moed="+request.moed+"&data="+request.dataURL+"&webc="+request.webc+"&last_exam="+request.last_exam;
        sendRequest(data, "https://coursesmarts.tk/V2/submit_hist_gr.php", sendResponse);
}

function postReview(request, sendResponse){
        var data = "course="+request.course+"&semester="+request.semester+"&year="+request.year+"&general="+request.general+"&load="+request.load+"&name="+request.username+"&userid="+userid;
        sendRequest(data, "https://coursesmarts.tk/V2/submit_rev_gr.php", sendResponse);
}

function postRating(request, sendResponse){
        var data = "course="+request.course+"&type="+request.rate_type+"&rating="+request.rating+"&userid="+userid+"&update="+request.update+"&old_rating="+request.old_rating;
        sendRequest(data, "https://coursesmarts.tk/V2/submit_rate_gr.php", sendResponse);
}

function postAvg(request, sendResponse) {
    if (request.part === '0'){
        var average = parseFloat(request.averages[0]);
        var median = parseFloat(request.medians[0]);
        var B_average = parseFloat(request.averages[1]);
        var B_median = parseFloat(request.medians[1]);
        var A_average = parseFloat(request.averages[2]);
        var A_median = parseFloat(request.medians[2]);
        var data = "course="+request.course+"&semester="+request.semester+"&year="+request.year+"&average="+average+"&median="+median+"&A_average="+A_average+"&A_median="+A_median+"&B_average="+B_average+"&B_median="+B_median+"&part="+request.part;
    } else {
        var average = parseFloat(request.averages);
        var median = parseFloat(request.medians);
        var data = "course="+request.course+"&semester="+request.semester+"&year="+request.year+"&average="+average+"&median="+median+"&moed="+request.moed+"&part="+request.part;
    }
    sendRequest(data, "https://coursesmarts.tk/V2/submit_avg_gr.php", sendResponse);
}

function getRating(request, sendResponse){
    var data="course="+request.course;
    sendRequest(data, "https://coursesmarts.tk/V2/get_rating.php", sendResponse);
}

function getAverage(request, sendResponse){
    var data="course="+request.course;
    sendRequest(data, "https://coursesmarts.tk/V2/get_average.php", sendResponse);
}

function getMishal(request, sendResponse){
    var url = "";
    for (i=0 ; i < request.urls.length; i++) {
        url = url + "__"+i+"__"+ request.urls[i].replaceAll("&", "|");
    }
    var data = "id="+request.id+"&pass="+request.pass+"&url="+url+"&count="+request.count;
    //console.log(url);
    sendRequest(data, "https://coursesmarts.tk/composer/mishal.php", sendResponse);
}

function getLecturerAvg(request, sendResponse){
    var data="lecturer="+request.lecturer+"&course="+request.course;
    sendRequest(data, "https://coursesmarts.tk/V2/get_lect_avg.php", sendResponse);
}

function verifyMishCreds(request, sendResponse){
	var data="verify=1&id="+request.id+"&pass="+request.pass;
	sendRequest(data, "https://coursesmarts.tk/composer/mishal.php", sendResponse);
}

function getSeatsLeft(request, sendResponse){
    var data="course="+request.course+"&id="+request.id+"&pass="+request.pass;
    //console.log(data);
    sendRequest(data, "https://coursesmarts.tk/composer/get_seats.php", sendResponse);
}

function reportErr(request, sendResponse){
    var data="message="+request.message;
    //console.log(data);
    sendRequest(data, "https://coursesmarts.tk/V2/report_js_error.php", sendResponse);
}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.type === "postHist"){
        postHistogram(request, sendResponse);
    }
    if (request.type === "postRev"){
        postReview(request, sendResponse);
    }
    if (request.type === "postRate"){
        postRating(request, sendResponse);
    }
    if (request.type === "identify"){
        identifyUser(request, sendResponse);
    }
    if (request.type === "postAvg"){
        postAvg(request, sendResponse);
    }
    if (request.type === "getRating"){
        getRating(request, sendResponse);
    }
    if (request.type === "getAverage"){
        getAverage(request, sendResponse);
    }
    if (request.type === "getMishal"){
        getMishal(request, sendResponse);
    }
    if (request.type === "getLecturerAvg"){
        getLecturerAvg(request, sendResponse);
    }
    if (request.type === "verifyMishCreds"){
        verifyMishCreds(request, sendResponse);
    }
    if (request.type === "getSeatsLeft"){
        getSeatsLeft(request, sendResponse);
    }
    if (request.type === "reportErr"){
        reportErr(request, sendResponse);
    }
    if (request.type === "getPastLecturers"){
        getPastLecturers(request, sendResponse);
    }
    return true;
});

